from django.shortcuts import render
from django.http import HttpResponse

# def index (request):
#     response = {'name' : 'Syauqi'}
#     return render (request, 'index.html', response)

def index (request):
    return render (request, 'mainbootstrap.html')

def about (request):
    return render (request, 'about.html')

def gallery (request):
    return render (request, 'gallery.html')


# Create your views here.
