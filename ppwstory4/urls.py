from django.urls import include, path
from . import views

app_name = 'ppwstory4'

urlpatterns = [
    path('', views.index, name = 'home'),
    path('about/', views.about, name = 'about'),
    path('gallery/', views.gallery, name = 'gallery'),
]